data_dir=data
cache_dir="$PUBNET_CACHE_DIR"
graph_name="pubnet"
destination_dir="$PUBNET_DATA_DIR"/"$GRAPH_NAME"
structure_file=structure.yml

cd $(dirname $0)

download_pubmed_data.sh --source updatefiles \
    --destination "$data_dir" \
    {1357..1361}

read_xml --structure-file "$structure_file" \
    --cache-dir "$cache_dir" \
    $data_dir/*

convert2graph.sh --structure-file "$structure_file" \
    --source "$cache_dir" \
    --destination "$destination_dir" \
    --clean
