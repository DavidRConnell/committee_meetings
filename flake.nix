{
  description = "Presentation repo for dissertation meetings";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils = { url = "github:numtide/flake-utils"; };
    pubnet-flake = {
      url = "/home/voidee/packages/python/pubnet/";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    a2g-flake = {
      url = "/home/voidee/packages/python/abstract2gene/";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, pubnet-flake, a2g-flake }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        python = pkgs.python3;
        pubnet = pubnet-flake.packages.${system}.pubnet;
        abstract2gene = a2g-flake.packages.${system}.abstract2gene;
      in {
        devShells.default = pkgs.mkShell {
          packages =
            [ (python.withPackages (p: [ p.ipython pubnet abstract2gene ])) ];
        };
      });
}
